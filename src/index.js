import React from 'react';
import ReactDOM from 'react-dom';
import  GifExpertApp  from "./components/GifExpertApp";

import './index.css';

const rootNode = document.getElementById('root');
ReactDOM.render(
    <GifExpertApp defaultCategories={['one punch']}/>,
    rootNode
);

// gifExpertApp
//<h2>GifExpertApp</h2>
//<hr/>

