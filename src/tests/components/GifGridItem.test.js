import {shallow} from 'enzyme';
import {GifGridItem} from '../../components/GifGridItem'

const title="Un titulo";
const url = "https://localhost:8888/imagen.jpg"
const wrapper = shallow(<GifGridItem url={url} title={title} />);

describe('pruebas sobre el componente GifGridItem', ()=>{
    test('validación de snapshot',()=>{   
        expect( wrapper ).toMatchSnapshot();
    });

    test('debe de tener un párrafo con el title', ()=>{
        const  p = wrapper.find('p');
        expect( p.text().trim() ).toBe( title );
    });


    test('debe de tener la imagen igual al url y alt de los props',()=>{
        const img = wrapper.find('img');
        expect( img.prop('src') ).toBe(url);
        expect( img.prop('title') ).toBe(title);

    });

    test('debe de tener animate__fadeIn', ()=>{
        const div = wrapper.find('div');
        const clase = 'animate__fadeIn';
        expect( div.prop('className').includes(clase) ).toBe(true);

    });
});