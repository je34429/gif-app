import React from 'react'
import { shallow } from "enzyme"
import {GifGrid} from '../../components/GifGrid';
import {useFetchGifs} from '../../hooks/useFetchGifs';

jest.mock('../../hooks/useFetchGifs');

describe('Evaluando componente GifGrid', () => {
   
    test('debe haber match con Snpashot', () => {
        
        useFetchGifs.mockReturnValue({ 
            data:[],
            loading:true 
        });

        const category = 'oe';
        const wrapper = shallow( <GifGrid category={category} />);
        expect(wrapper).toMatchSnapshot();
    });

    test('debe de mostrar items cuando se cargan imágenes useFetchGifs', () => {
        const gifs=[{
            id:'ABC',
            url:'https://www.oe.com/img.jpg',
            title:'titulo'
        },
        {
            id:'123',
            url:'https://www.oe.com/img.jpg',
            title:'titulo'
        }];

        useFetchGifs.mockReturnValue({ 
            data:gifs,
            loading:false 
        });

        const category = 'oe';
        const wrapper = shallow( <GifGrid category={category} />);
        expect(wrapper).toMatchSnapshot();
        //el p del loading no debe existir
        expect(wrapper.find('p').exists()).toBe(false);
        expect(wrapper.find('GifGridItem').length).toBe(gifs.length);

    });
    
    
})
