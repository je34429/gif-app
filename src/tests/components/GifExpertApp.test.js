import React from 'react'
import { shallow } from 'enzyme'
import GifExpertApp from '../../components/GifExpertApp'

describe('pruebas para el componente GifExpertApp', () => {
    test('debe hacer match con el snapshot', () => {
        const wrapper = shallow(<GifExpertApp/>);
        expect(wrapper).toMatchSnapshot();
    });
    test('debe mostrar una lista de categorias ', () => {
        const categories = ['one punch', 'dragon ball'];
        const wrapper = shallow(<GifExpertApp defaultCategories={categories} />);
        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find('GifGrid').length).toBe(categories.length);
    })
})
