import { shallow } from "enzyme"
import { AddCategory } from "../../components/AddCategory";

describe('pruebas sobre el componente <AddCategory/>', () => {
    const setCategories = jest.fn();
    let wrapper;
    beforeEach(()=>{
        jest.clearAllMocks();
        wrapper = shallow(<AddCategory setCategories={setCategories}/>);
    });

    test('debe de mostrarse correctamente ', () => {    
        expect(wrapper).toMatchSnapshot();
    });

    test('debe de cambiar la caja de texto', () => {
        const input = wrapper.find('input');
        input.simulate('change',{target:{value:'oe'}});
    });
    
    test('no debe de postear la información en submit', () => {
        
        wrapper.find('form').simulate('submit', {
            preventDefault(){} 
        });
        
        expect(setCategories).not.toHaveBeenCalled();
    });

    test('debe llamar el set categories y limpiar la caja de texto', () => {
        
        const input = wrapper.find('input');
       
        //simular el inputchange
        input.simulate('change', {target:{value:'oe'}});

        //simular el submit
        wrapper.find('form').simulate('submit',{
            preventDefault(){} 
        });
        //setCategories se debe haber llamado
        expect(setCategories).toHaveBeenCalledWith( expect.any(Function) );
        //el valor del input debe ser ''
        expect(input.prop('value')).toBe('');

                
    })
    
    
})
