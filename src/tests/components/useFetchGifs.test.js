import { useFetchGifs } from "../../hooks/useFetchGifs"
import { renderHook } from '@testing-library/react-hooks'

describe('preubas en el custom hook useFetchGifs', () => {
    test('debe de retornar el estado inicial', async() => {
        
        const { result, waitForNextUpdate } = renderHook(()=>useFetchGifs('One Punch'));
        const {data, loading } = result.current;
        console.log(data,loading);

        await waitForNextUpdate();

        expect(data).toEqual([]);
        expect(loading).toBe(true);
    });

    test('debe retornar un arreglo de imagenes y el loading en false', async() => {
        
        const { result, waitForNextUpdate } = renderHook(()=>useFetchGifs('One Punch'));
        await waitForNextUpdate();
        const {data, loading } = result.current;
        expect(data.length).toBeGreaterThan(0);
        expect(loading).toBe(false);
    })
})
