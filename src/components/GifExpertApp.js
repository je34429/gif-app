import React, { useState } from 'react';
import { AddCategory, GifGrid } from '../components';


const GifExpertApp = ({defaultCategories=[]})=>{

    //const categories =['One punch', 'Samurai X', 'Dragon Ball'];
   const [categories, setCategories] = useState(defaultCategories);

/*
    const handleAdd=()=>{
        setCategories([...categories,'yo']);
        //setCategories(prevState => [...prevState, 'yo']);
    }
*/

const onAddCategory = (newCategory) => {
    if(categories.includes(newCategory))return;
    
    setCategories([...categories,newCategory]);
}
    

    return  (
                <>
                    <h2>GifExpertApp</h2>
                    <AddCategory 
                        onNewCategory = { (val)=>onAddCategory(val) }
                    />
                    <hr/>
                    
                    {
                        categories.map((category) => {
                            return (<GifGrid category = {category} key = {category.toString()} />);
                        })
                     }
                    
                </>
            );
};

export default GifExpertApp;